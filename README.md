## Phing Build Script

Phing build script for my Symfony project.

### Requirements
1. Git
1. Yarn

### Usage
```
php phing.phar
```
